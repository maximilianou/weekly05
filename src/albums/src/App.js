// App.js
import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import DashboardPage from './pages/DashboardPage';
import AlbumsPage from './pages/AlbumsPage';
const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path='/' component={DashboardPage} />
        <Route exact path='/albums' component={AlbumsPage} />
        <Redirect to='/' />
      </Switch>
    </Router>
  )
};
export default App;