// components/Album.js
import React from 'react';
import { Link } from 'react-router-dom';
export const Album = ({ album }) => (
    <article className='album-excerpt' >
        <h2>{album.title}</h2>
        <p>{album.body.substring(0, 100)}</p>
    </article>
)