# weekly05

learning public

https://weeklyproject.club/

Happy Monday Maximiliano!
Quinn is the lead singer in an Indie Rock band called Trinkets for Miles.  They play a lot of small venues and coffee shops, where during the performance they sell CDs, cassettes, and vinyls of their albums.  They’re having trouble keeping track of which albums and media types are most popular at which venues.  Make something for Trinkets for Miles that can:

Keep track of what venue they’re currently playing
Record which album on what type of media has been purchased
Enter new albums and new types of media
Report which albums and which media are more popular at each venue

If you want to go the extra mile, make it look retro to fit in with the band's aesthetic. 
 

Don't forget to plan out your week here, and you can always submit a help request here.
-Sam

Reference:

https://www.taniarascia.com/redux-react-guide/

------

```
npx create-react-app albums
cd albums
npm i redux react-redux redux-thunk redux-devtools-extension react-router-dom
cd src && rm *
mkdir actions components pages reducers
touch index.js index.css App.js
```

```
// index.js
// External Imports
import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware} from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
// Local Imports
import App from './App';
import rootReducer from './reducers';
// assets
import './index.css';
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.querySelector('#root')
);
```

```
// App.js
import React from 'react';
const App = () => {
  return <div>Hello, Redux!!!</div>
};
export default App;
```



--------
